\documentclass{llncs}

\usepackage[T1]{fontenc}
\usepackage{graphicx}
\graphicspath{{figures}}
\usepackage{tikz}
\usetikzlibrary{arrows}
\usepackage{mathrsfs}

\usepackage{hyperref}
\hypersetup{
  pdfauthor = {Nikolaos Pothitos},
  pdftitle = {Constraint Programming: Algorithms and
              Systems},
  pdfsubject = {PhD Thesis Summary},
  pdfkeywords = {search, heuristics, randomization,
                 MapReduce, constraint propagation, bounds
                 consistency, maintaining arc consistency}
}
% Land links to the top of a figure instead of caption:
\usepackage[all]{hypcap}
% If you use the hyperref package, please uncomment the
% following two lines to display URLs in blue roman font
% according to Springer's eBook style:
\usepackage{color}
\renewcommand\UrlFont{\color{blue}\rmfamily}

\begin{document}

\title{Constraint Programming: Algorithms and Systems}

\author{Nikolaos Pothitos\thanks{Dissertation Advisor:
        Panagiotis Stamatopoulos, Assistant Professor}}

\institute{Department of Informatics and Telecommunications \\
           National and Kapodistrian University of Athens \\
           Panepistimiopolis, 157\,84 Athens, Greece \\
           \email{pothitos@di.uoa.gr}}

\maketitle

\begin{abstract}
  \emph{This paper is a summary of the dissertation of the
  same title.} Constraint Programming aims at the easy
  declaration and fast resolution of Constraint Satisfaction
  Problems (CSPs) like course scheduling and radio link
  frequency assignment. To solve these problems, Constraint
  Programming employs search methods and constraint
  propagation. This dissertation contributes on both. (i) We
  develop novel efficient search methods that are based on
  new heuristics. These new heuristics implement the
  \emph{gradual} randomization of deterministic heuristics.
  (ii) We demonstrate how the MapReduce framework can be
  used for speeding up and distributing the search of a CSP
  solution to all the available solvers-workers. (iii) We
  highlight the advantages of relaxed constraint propagation
  levels like \emph{bounds consistency} in comparison to
  higher levels like \emph{arc consistency}. We propose new
  relaxed constraint propagation levels, and we compare
  their performance to higher propagation levels, both in
  theory and practice. We answer the question about when it
  is worth to utilize relaxed constraint propagation levels.
  Our contributions were tested using mostly CSPs that occur
  in the real world and a wide range of CSPs included in
  official Constraint Programming solvers competitions.

  \keywords{search \and heuristics \and randomization \and
  MapReduce \and constraint propagation \and bounds
  consistency \and maintaining arc consistency}
\end{abstract}

\noindent
The motto of Constraint Programming is ``the user simply
states the problem and the computer solves
it''~\cite{Freuder1997}. This proposition implies that
\begin{itemize}
  \item the ``user'' is required to provide only a bare
        minimum of the description of a problem, i.e.\ only
        the \emph{constraints} should be defined, and
  \item the rest (solution search process) is undertaken by
        the machine, i.e.\ solver.
\end{itemize}
There are several variations of the solution search
processes under the Constraint Programming umbrella. In all
cases, however, we describe and formalize every problem as a
\emph{Constraint Satisfaction Problem} (CSP).

\begin{definition}
  A \emph{Constraint Satisfaction Problem (CSP)} consists of
  \begin{itemize}
    \item a set of constrained \emph{variables} $\mathscr{X}
          = \{X_1, X_2, \ldots, X_n\}$,
    \item the corresponding set of \emph{domains}
          $\mathscr{D} = \{D_1, D_2, \ldots, D_n\}$ which
          are finite sets (of integer values in this work)
          and
    \item the set of \emph{constraints} between the
          variables $\mathscr{C} = \{C_1, C_2, \ldots,
          C_e\}$. Each constraint is defined as $C_i = (S_i,
          R_i)$.
          \begin{itemize}
            \item $S_i$ is the subset of $\mathscr{X}$
                  containing the variables affected by the
                  constraint.
            \item $R_i$ contains all the valid combinations
                  of the values of the domains of the
                  variables in $S_i$. Formally, $R_i
                  \subseteq D_{i_1} \times D_{i_2} \times
                  \cdots \times D_{i_k}$, with $X_{i_1},
                  X_{i_2}, \ldots, X_{i_k} \in S_i$ and $i_1
                  < i_2 < \cdots < i_k$.
          \end{itemize}
  \end{itemize}
\end{definition}

The description of any CSP is a minimal definition of
\emph{what} the problem is and does \emph{not} contain
information on \emph{how} to solve it. As illustrated in
Fig.~\ref{outline}, the CSP definition and resolution are
two different and independent phases.

\begin{figure}
  \centering
  \begin{tikzpicture}
    [->, >=stealth', thick]

    \node at (0,0)  (CP) {\parbox{6em}{Constraint\\Programming}};

    \node at (3,+2)  (CSP) {\parbox{10em}{Constraint Satisfaction\\Problem Definition}};
    \node at (3, 0)  (PHASES) {\textrm{\emph{\color{blue} independent phases}}};
    \node at (3,-2)  (SEARCH) {Search Method for Solving};

    \node at (7,+3)  (VARIABLES) {Variables};
    \node at (7,+2)  (DOMAINS) {Domains};
    \node at (7,+1)  (CONSTRAINTS) {Constraints};

    \path
      (CP)   edge  (CSP)
      (CP)   edge  (SEARCH)

      (PHASES)  edge [dashed,blue]  (CSP)
      (PHASES)  edge [dashed,blue]  (SEARCH)

      (CSP)  edge  (VARIABLES)
      (CSP)  edge  (DOMAINS)
      (CSP)  edge  (CONSTRAINTS)
      ;
  \end{tikzpicture}
  \caption{Constraint Programming outline}
  \label{outline}
\end{figure}

An example of a Constraint Satisfaction Problem from the
real world is the creation of timetables for educational
institutes (course scheduling). In this kind of problems,
there are hard constraints such as two courses cannot be
taught simultaneously in the same room. Softer constraints
have to do with the absence of gaps in the personal
timetables of professors and students.

Another example of a Constraint Satisfaction Problem is the
allocation of radio frequencies in a country so that no
radio station interferes with another (radio link frequency
assignment problem). A hard constraint in this case is that
two stations which are broadcast to the same area must use
frequencies that differ by at least a certain number of
megacycles.

\subsubsection{Binary Constraints.}

Each binary constraint of a CSP affects exactly \emph{two}
variables. A binary CSP is a CSP containing only binary
constraints. Binary CSPs are important because it has been
proven that any non-binary CSP can be transformed into an
equivalent binary one~\cite{Rossi1990}. And it is easier, at
least in theory, to manipulate a binary constraint rather
than a higher-level constraint.

Let us assume that for a given binary CSP there is a unique
constraint $C_k = (S_k, R_k)$ with $S_k = \{X_i, X_j\}$.
Then, for the sake of readability, we denote this $C_k$
constraint as $C_{ij}$, which in turn is equivalent to
$C_{ji}$.

\subsubsection{Contributions.}

In Constraint Programming we solve CSPs by interchanging
\begin{itemize}
  \item a search method and
  \item a constraint propagation process.
\end{itemize}
In this dissertation, we contribute on both by developing
\begin{enumerate}
  \item the gradual randomization of
        heuristics~\cite{Pothitos2016-PoPS,Pothitos2018}
  \item the distribution of search in the context
        MapReduce~\cite{Pothitos2016-CPMR}
  \item the constraint propagation relaxation for the
        benefit of
        efficiency~\cite{Pothitos2020,Pothitos2012-Scheduling}
\end{enumerate}
as summarized in the three sections that follow.

\begin{figure}
  \centering
  \includegraphics[width=\linewidth]{ContributionsMindMap}
  \caption{Our contributions}
\end{figure}

\section{Hybrid Random and Deterministic Heuristics}

A Constraint Satisfaction Problem consists of a set of
\emph{variables} and a set of \emph{constraints} that
connect them. A combination of the values of the variables
that does not violate any constraint is a \emph{solution} of
the problem. To find a solution, it is enough to check all
the possible combinations of the values of the variables.

\subsection{Why Heuristics Matter}

However, as the variables of the problem increase, the
combinations to consider increase exponentially. As the
number of candidate solutions is prohibitive to examine them
all, we need the so-called \emph{heuristics} in order to
examine first the most ``promising'' candidate solutions.

If there were a perfect heuristic rule, then there would be
no need to use a search algorithm: One would give all
candidate solutions as input to the heuristic rule, and the
heuristic would return the solution directly. Nevertheless,
a perfect heuristic does not exist. This means that, in an
extreme but not rare case, a heuristic rule may give
priority to poor candidate solutions, thus delaying search.
This is why sometimes the search method randomly selects the
next candidate solution, instead of using a deterministic
heuristic rule.

\subsection{Gradual Randomization of Heuristics}

Between the two extremes of deterministic and random
heuristics, we introduce in the context of this thesis for
the first time the \emph{gradual} randomization of the
deterministic heuristic rules. Our hybrid heuristics were
constructed and tested for solving difficult real-world
Constraint Satisfaction Problems.

In addition, a new search method PoPS (Piece of Pie Search)
was created in order to effectively use the above hybrid
heuristic rules. While the search method makes a small
number of assignments, it uses random heuristics. But while
the number of assignments increases, our search method
gradually makes the heuristics more
deterministic~\cite{Pothitos2016-PoPS,Pothitos2018}. It is
like starting a game of chess. First, we have more options
and we can choose randomly from a larger set of moves. But
as the game goes on, our strategy becomes more and more
deterministic.

\section{Integrating MapReduce into Constraint Programming}

What we are asking for is not always to limit the complexity
of an algorithm so that it requires fewer computing
resources. These days, the question is how to exploit a
plethora of cheap commodity available computers. In our
case, let us imagine that we have at our disposal a large
number of machines in the Google or Amazon cloud. How can we
utilize them to resolve faster a Constraint Satisfaction
Problem?

MapReduce is a generic framework for the distribution and
mass processing of big data on an arbitrarily large number
of computers. It was originally introduced to index the
whole Internet. MapReduce is the basis of Google search
engine~\cite{Dean2004}. But, apart from this, MapReduce
framework has been used in many other applications such as
document clustering, machine learning, and automatic
translation.

One of the contributions of the thesis concerns the
integration of MapReduce into Constraint Programming. In
more detail, the subsets of the candidate solutions of a CSP
are encoded in a big text file, which is then fragmented
into pieces. These pieces are then sent to the
solvers-workers (mappers) to search whether there is a
solution inside them. In other words, we split the search
tree into several chunks which are automatically distributed
through MapReduce to a number of workers, which in turn
explore them simultaneously.

As MapReduce naturally accepts text files as input, we
propose a fast way of encoding the pieces of the search tree
in a large text file. Another goal is to cut the tree into
pieces of as equal sizes as possible to achieve a fair
distribution of the burden of work to the solvers-mappers.
It is inefficient to traverse a priori the entire search
tree in order to see its structure and cut it into equal
parts. That is why we sample only some of the nodes of the
tree to outline its structure, without the need to visit all
its nodes beforehand~\cite{Pothitos2016-CPMR}.

\section{The Revenge of Bounds Consistency}

So far, we have discussed about building new heuristics,
search methods, and how they are distributed via MapReduce.
Apart from the search methods, the other integral part of
Constraint Programming is the so-called \emph{constraint
propagation}.

\subsection{Search Methods and Constraint Propagation}

A systematic search method solves a CSP step by step. At
first, it assigns a value to the first constrained variable
of the problem out of its domain. Secondly, it assigns a
value to the second variable of the problem. Then, a value
is assigned to the third variable, and so on. After each
assignment, if any constraint is violated, it makes no sense
to keep this assignment and continue, pointlessly, to the
next assignment. In this case, the search method must undo
the invalid assignment and try an alternative.

The purpose of \emph{constraint propagation} is to
anticipate invalid assignments \emph{before} making them.
This is not achieved by simply checking---as in a systematic
search method---if the existing assignments violate any
constraint. Constraint propagation proactively removes
inconsistent values out of the domains of the unassigned
variables a priori, without the need to make an assignment
to them first.

In other words, constraint propagation does not only
consider existing assignments; it also tries to remove as
many nogood values as possible before they participate in a
future assignment. Generally speaking, the more inconsistent
(nogood) values are removed out of the domains, the higher
consistency (propagation) level we have.

\subsection{How Much Constraint Propagation?}

One could reasonably argue that the higher the consistency
level, the better for the search methods, as they will be
more efficient by skipping pointless assignments. However,
by increasing the consistency level, we inevitably increase
the time that constraint propagation takes. It is evident
that there is a trade-off here.

Recent research focuses on devising higher propagation
levels (Higher-Level Consistencies). Nevertheless, the point
is not just to move the search method cost to constraint
propagation, but to reduce the time required to solve a CSP
\emph{overall}.

\subsection{The Advantages of Lower Consistency Levels}

In this thesis, we present extensive experiments with our
Constraint Programming \textsc{Naxos Solver} for a wide
range of CSPs. We were able to compare the performance of
various constraint propagation levels, such as arc
consistently, bounds consistency, and a new
``$k$-bounds-consistency'' level. This novel level enforces
bounds consistency only to variables having domain
cardinality less than or equal to the $k$
parameter~\cite{Pothitos2012-Scheduling}.

We track for the first time the cases when maintaining
bounds consistency, a lower propagation level, is more
efficient than maintaining arc consistency, a higher
propagation level. These observations alone are important,
because there is a common belief that higher consistency
levels are always better.

Beyond our empirical observations---that will be shortly
presented in this summary paper too---the thesis
theoretically explains why some CSPs are solved more
efficiently while maintaining bounds instead of arc
consistency. We study separately the complexities of the two
consistency levels when integrated into a search method.
More importantly, for each CSP, we propose a criterion to
choose the right consistency level \emph{before} beginning
to solve it~\cite{Pothitos2020}.

\subsection{Background}

Consistency is a particularly useful property in the road to
solve a CSP. It implies that the values of the domains of
each variable have a kind of \emph{support} with respect to
the CSP constraints.

\begin{definition}
  An arc $(X_i, X_j)$ is \emph{arc consistent} iff for each
  $v_i \in D_i$ there exists a $v_j \in D_j$ with $(v_i,
  v_j)$ not violating $C_{ij}$.
\end{definition}

\begin{example}
  \label{AC}
  Let $X_1$ and $X_2$ be two constrained variables with
  domains $D_1 = \{1, 2, 3\}$ and $D_2 = \{2, 3, 4, 5, 6,
  7\}$. Let us assume that the constraint between the
  variables is $X_2 = 2 X_1$.

  $(X_1, X_2)$ is arc consistent, as for each of the values
  $1$, $2$, $3$ in $D_1$, the corresponding values $2$, $4$,
  $6$ belong to $D_2$.

  On the other hand, $(X_2, X_1)$ is \emph{not} arc
  consistent. To prove this, we need just one value from
  $D_2$ that does not have any support in $D_1$. Indeed, for
  the value $3$ in $D_2$, there is not any $v_1$ in $D_1$
  with $2 v_1 = 3$.

  If we want to make $(X_2, X_1)$ arc consistent, we should
  remove the values $3$, $5$, $7$ out of $D_2$ as they do
  not have any supports in $D_1$.

  This example also illustrates that consistency is not a
  symmetric property.
\end{example}

In order to check if an arc $(X_i, X_j)$ is arc consistent,
we have to iterate through all the values of $D_i$. The
function that does this and removes the unsupported values
from $D_i$ is called \textsc{Revise}. A faster yet looser
alternative would be to check if the arc is \emph{bounds}
consistent.

\begin{definition}
  \label{BC}
  An arc $(X_i, X_j)$ is \emph{bounds consistent} iff for
  the $\min D_i$ and $\max D_i$ values, there exist some
  $v_a, \, v_b \in D_j$ with $(\min D_i, v_a)$ and $(\max
  D_i, v_b)$ not violating $C_{ij}$.
\end{definition}

In this case, \textsc{Revise} has to check and update only
the two bounds of $D_i$. But, in the worst case, when no
support is found, it has to iterate through all $D_i$ values
too.

\begin{example}
  Again, let $X_1$ and $X_2$ be two variables with $D_1 =
  \{1, 2, 3\}$, $D_2 = \{2, 3, 4, 5, 6, 7\}$, and $X_2 = 2
  X_1$.

  $(X_1, X_2)$ is bounds consistent, as for each of the
  bounds $1$ and $3$ in $D_1$, the corresponding values $2
  \cdot 1 = 2$ and $2 \cdot 3 = 6$ belong to $D_2$.

  Nevertheless, $(X_2, X_1)$ is bounds inconsistent, as the
  upper bound $7$ of $D_2$ has not any support in $D_1$.

  If we want to enforce bounds consistency to $(X_2, X_1)$,
  we should remove $7$ out of $D_2$. Note that only one
  removal is needed in the case of bounds consistency
  enforcement in contrast to the three removals needed by
  the arc consistency enforcement for the same domains in
  Example~\ref{AC}.
\end{example}

\subsection{Related Work}

Starting from the 70's, the research on constraint
propagation goes hand in hand with Constraint Programming
research~\cite{Mackworth1997,Waltz1975}. Throughout all
these years, there is a trend to invent stronger and
stronger constraint propagation methodologies. There are
propagation methodologies tailor-made for local
search~\cite{Pothitos2012-CBLS}. Nevertheless, the focus of
constraint propagation research is on constructive search
methods.

\subsubsection{Learning from Mistakes or Preventing Them?}

\emph{Look back} techniques in backtracking search methods
aim to avoid repeating the invalid assignments of the past.
\emph{Backjumping} is a well-known look back technique, but
it is not used in solvers, as other techniques clearly
outperform it even in simple CSPs~\cite{Ayub2017}.
\emph{Nogood learning} is a more promising look back
technique that, based on the invalid assignments of the
past, adds new constraints to avoid the invalid combination
of assignments in the future~\cite{Veksler2016}. However,
these new constraints have the drawback of making the
constraint network increasingly complex.

On the other hand, \emph{look ahead} techniques are more
proactive in the sense that they remove values out of the
domains of the constrained variables \emph{before} reaching
an inconsistent assignment. \emph{Maintaining arc
consistency} (MAC) during search is the queen of all look
ahead techniques~\cite{Bessiere2020}. According to MAC, each
assignment to a domain of a variable is followed by an arc
consistency enforcement method, such as the known optimal
AC-2001 algorithm~\cite{Bessiere2005}. The optimality of
AC-2001 was proven for enforcing arc consistency after a
single assignment. But when we call repeatedly AC-2001
during search, after each single assignment, in order to
\emph{maintain} arc consistency, there is still room for
improvements~\cite{Li2019}.

\subsubsection{The Importance of Arc Consistency.}

Arc consistency also plays a key role in splitting the CSPs
into two large categories~\cite{Bessiere2020}.
\begin{enumerate}
  \item The \emph{tractable} ones that can be solved in
        polynomial time, simply by maintaining arc
        consistency.
  \item The \emph{intractable} ones that are NP-complete
        problems and require an exponential backtracking
        algorithm to prove whether they have a solution or
        not.
\end{enumerate}
Related work has defined the properties of the constraint
network that suffice to categorize a CSP as tractable or
intractable~\cite{Cohen2017}. Furthermore, it has been
recently proven that a CSP is tractable only if it contains
specific types of constraints~\cite{Bulatov2017,Zhuk2017}.

\subsubsection{Higher-Level Consistencies.}

Arc consistency (AC) filters many futile values out of the
domains of the constrained variables. But there are even
stronger consistency levels than arc consistency. These are
the so-called \emph{higher-level consistencies} (HLCs) and,
while AC examines one constraint at a time, HLCs consider
two or more constraints simultaneously. This makes them too
expensive to be used in practice~\cite{Balafrej2016}.

To mitigate the HLC overhead, there are hybrid strategies
that go back and forth from HLC to AC~\cite{Woodward2018}.
Even machine learning has been employed to dynamically
choose which consistency level is more
efficient~\cite{Balafrej2015}. Furthermore, the higher-level
consistencies (HLCs) themselves are rarely used in practice
for two main reasons.
\begin{itemize}
  \item HLC methodologies require extra implementation
        effort in order to be used in Constraint Programming
        solvers, and they are usually complicated.
  \item HLC is not only complex in terms of implementation,
        but it also costs in terms of time. It is not a
        secret anymore that if we compare AC versus HLCs for
        a wide range of problems, AC is usually
        faster~\cite{Stergiou2021}.
\end{itemize}
According to recent related works, the remedy to mitigate
this unexpected behavior is to employ an HLC instead of AC
on the fly, only under specific conditions.

\subsubsection{Toward More Relaxed Consistencies.}

In this work, we are moving toward the opposite direction,
and instead of inventing an HLC and then trying to enforce
it in practice only under certain conditions, we invest on
bounds consistency (BC), a looser consistency level than arc
consistency (AC).

We do not change consistency levels on the fly. We stick to
one consistency level at a time (AC or BC) in order to keep
the overall search algorithm that maintains consistency as
simple as possible. This enables us to shed a more
theoretical light to the integration of consistency into
search and study the overall consistency complexities, not
isolated but always \emph{in the context of search methods}
that maintain them. Our computations are backed by wide
experimental data. Instead of swapping HLCs and AC, we
choose AC and BC, as bounds consistency is naturally used to
describe constraints in Constraint Programming
solvers~\cite{Lallouet2020}.

\subsection{AC versus BC Empirical Evaluations}

We consider all standard CSP instances taken from the First
XCSP3 Constraint Mini-Solver Competition~\cite{XCSP2017}.
The specific instances used in the \emph{mini-solver} track
are available under the respective link in the competition
site.\footnote{\url{http://www.cril.univ-artois.fr/XCSP17}}

In order to make comparisons, we had to employ two different
solvers: one that maintains arc consistency (AC) and another
that maintains bounds consistency (BC). Therefore, we took
the open source \textsc{Naxos Solver}~\cite{Naxos} and
created its AC and BC variants. Note that the original
\textsc{Naxos Solver} implements several consistency levels
for various constraints. Consequently, we created two sets
of patches, one that implements pure arc consistency and
another for pure bounds consistency for every constraint
employed. All patches are freely
available.\footnote{\url{https://github.com/pothitos/ACvsBC-Solver-Patches}}

Using the above methodology, we created two separate
solvers, one for AC and one for BC. Each of them was
assigned to solve the First XCSP3 Constraint Mini-Solver
Competition CSPs~\cite{XCSP2017}. Each CSP instance has to
be solved within 40~minutes according to the competition
standards. Please note that only the CSP instances that were
solved at least from one solver were taken into
consideration.

We executed the experiments in an Ubuntu Linux 18.04 virtual
machine with 8 virtual CPUs and 8\,GB of memory.

\newcommand{\rhombus}{{\Large$\diamond$}}
\newcommand{\triangleUp}{$\scriptstyle\bigtriangleup$}
\newcommand{\triangleDown}{\raisebox{0.15em}{$\scriptstyle\bigtriangledown$}}

\subsubsection{Visualization.}

In order to make comparisons more easily, we depicted
graphically the ratio $\mathrm{TIME_{AC}} /
\mathrm{TIME_{BC}}$ versus $d / n$ in Figure~\ref{XCSP3}
using the \rhombus{} symbol, where $n$ is the number of
constrained variables and $d$ is the maximum cardinality of
the domains of the variables of each CSP.

When the AC solver does not produce a solution, the
corresponding point in the figure is depicted with a
\triangleUp{} symbol. This represents a high
$\mathrm{TIME_{AC}} / \mathrm{TIME_{BC}}$ ratio, which means
that maintaining BC is much more efficient than AC in this
case.

On the other hand, when the BC solver does not produce a
solution, the ratio $\mathrm{TIME_{AC}} /
\mathrm{TIME_{BC}}$ is depicted with a \triangleDown{}
symbol, which denotes a very low ratio and means that AC is
much more efficient than BC in this case.

It may be obvious that the above \triangleUp{} and
\triangleDown{} points do not correspond to real values.
They are used in the margins of Figure~\ref{XCSP3} to
represent marginal ratios, as described above.

\begin{figure}[t]
  \centering
  \input{figures/XCSP3-summary}
  \caption{The time needed to solve the CSPs while
           maintaining AC divided to the time spent while
           maintaining BC}
  \label{XCSP3}
\end{figure}

As the \rhombus{} points in the figure are somehow sparse,
the results become more intuitive if we draw a smooth curve
between them. Therefore, the curve in Figure~\ref{XCSP3} has
been derived by the LOESS
method~\cite{Cleveland1991,Wilcox2017} and is representative
of the~\rhombus{} points. In rough lines, LOESS is used to
unify scattered points along the plot by drawing a smooth
curve that passes between them. The advantage of this method
is that it does not require a parameter or function of any
form to fit a model to the data. The only input is the data
themselves. In our case, we made LOESS method ignore the
marginal \triangleUp{} and \triangleDown{} points because
they do not depict real values.

\subsubsection{Observations.}

In Figure~\ref{XCSP3} we compare the times for solving a CSP
instance via maintaining AC and BC. A first conclusion is
that BC can be better than AC for many instances. This is an
important observation, as, due to the fact that AC enforces
a stronger consistency level than BC, and both AC and BC
have equal worst-case complexities, there is the
misconception that AC is always better than BC.

However, the conclusion about the occasional superiority of
BC over AC has no practical use, if we do not know when it
happens. We have to find the appropriate conditions to know
a priori if a CSP instance will be solved faster by
maintaining AC or BC.

To put it simply, the $d / n$ ratio affects the
$\mathrm{TIME_{AC}} / \mathrm{TIME_{BC}}$ ratio, and this is
evident in practice in Figure~\ref{XCSP3}: On average,
$\mathrm{TIME_{AC}} / \mathrm{TIME_{BC}} < 1$ if $d / n < 1$
and $\mathrm{TIME_{AC}} / \mathrm{TIME_{BC}} > 1$ if $d / n
> 1$. This becomes clearer if we observe the smooth curve
constructed by the LOESS method, which represents the
``average'' of the \rhombus{} points~\cite{Cleveland1991}.

Regarding the \triangleUp{} points (that represent the cases
when only the maintaining BC method found a solution while
maintaining AC did not find one) they are apparently more on
the right side, i.e.\ when $d / n > 1$. On the other hand,
the~\triangleDown{}~points are gathered mostly on the left
side of Figure~\ref{XCSP3}. This means that for $d / n < 1$,
the maintaining BC methodology is usually not only less
efficient than AC, but it may produce no solution for a CSP,
while AC is able to solve it.

\section{Conclusions}

``The user simply states the problem and the computer solves
it''~\cite{Freuder1997}. This is the motto of the scientific
field of Constraint Programming, and this is the target of
this thesis: the better user experience through the faster
resolution of real-world Constraint Satisfaction Problems.
Our contributions are summarized in
\begin{enumerate}
  \item the combination of random and deterministic
        heuristic rules to speed up
        search~\cite{Pothitos2016-PoPS,Pothitos2018}
  \item the partitioning and distribution of the search
        space into many workers via the MapReduce
        framework~\cite{Pothitos2016-CPMR}
  \item the theoretical and experimental study of the
        benefits that the constraint propagation relaxation
        can
        offer~\cite{Pothitos2020,Pothitos2012-Scheduling}
\end{enumerate}
This paper summarized all three contributions and presented
the related work and empirical evaluations for the third
one. In the future, one can extend these empirical results
by taking into consideration more consistency levels other
than arc and bounds consistency.

\nocite{Pothitos2010-Domains}

\bibliographystyle{splncs04}
\bibliography{references}

\end{document}
